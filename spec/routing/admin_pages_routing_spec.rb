require "spec_helper"

describe AdminPagesController do
  describe "routing" do

    it "routes to #index" do
      get("/admin_pages").should route_to("admin_pages#login")
    end

    it "routes to #/admin_pages/subjects" do
      get("/admin_pages/subjects").should route_to("admin_pages#/subjects")
    end

    it "routes to #/subjects/new" do
      get("/admin_pages/subjects/new").should route_to("admin_pages#/subjects/new")
    end

    it "routes to #/subjects/edit" do
      get("/admin_pages/subjects/edit/1").should route_to("admin_pages#/subjects/edit", :id => "1")
    end

    it "routes to #/subjects/destroy" do
      delete("/admin_pages/subjects/1").should route_to("admin_pages#/subjects/destroy", :id => "1")
    end

    it "routes to #create" do
      post("/admin_pages").should route_to("admin_pages#create")
    end

    it "routes to #update" do
      put("/admin_pages/1").should route_to("admin_pages#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/admin_pages/1").should route_to("admin_pages#destroy", :id => "1")
    end

  end
end