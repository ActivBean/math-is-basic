require 'spec_helper'

describe Lesson do
  before do
    @lesson = Lesson.new(name: "Lesson1", level: 1, minLessonTemplateLevel: 1, maxLessonTemplateLevel: 4)
  end

  subject { @lesson }

  it { should respond_to(:name) }
  it { should respond_to(:level) }
  it { should respond_to(:minLessonTemplateLevel) }
  it { should respond_to(:maxLessonTemplateLevel) }

  it { should be_valid }

  describe "when name is not present" do
    before { @lesson.name = " " }
    it { should_not be_valid }
  end
end
