require 'spec_helper'

describe LessonTemplate do
  before do
    @lesson_template = LessonTemplate.new(name: "Template1", templateLevel: 1, body: "")
  end

  subject { @lesson_template }

  it { should respond_to(:name) }
  it { should respond_to(:templateLevel) }
  it { should respond_to(:body) }

  it { should be_valid }

  describe "when name is not present" do
    before { @lesson_template.name = " " }
    it { should_not be_valid }
  end
end
