require 'spec_helper'

describe Subject do
   before do
    @subject = Subject.new(name: "TestSubject", minLessonLevel: 1, maxLessonLevel: 4)
  end

  subject { @subject }

  it { should respond_to(:name) }
  it { should respond_to(:minLessonLevel) }
  it { should respond_to(:maxLessonLevel) }

  it { should be_valid }

  describe "when name is not present" do
    before { @subject.name = " " }
    it { should_not be_valid }
  end
end
