# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :lesson_template do
    name "MyString"
    templateLevel 1
    body ""
    lesson nil
  end
end
