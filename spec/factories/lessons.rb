# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :lesson do
    name "MyString"
    level 1
    maxLessonTemplateLevel ""
    minLessonTemplateLevel ""
    subject nil
  end
end
