require 'spec_helper'

describe "UserPages" do

	describe "Main page" do
		it "should have h1 'Administor the Application'" do
			visit '/users'
			page.should have_selector('h1', :text => 'Administor the Application')
		end	
	 	it "should have the title 'Home'" do
			visit '/admin_pages/home'
			page.should have_selector('title', :text => ' | Home')
	 	end	
	end 

	describe "Subjects page" do
		it "should have h1 'Add Subject Information'" do
			visit '/admin_pages/subjects'
			page.should have_selector('h1', :text => 'Add Subject Information')
		end
		it "should have the title 'Subjects'" do
			visit '/admin_pages/subjects'
			page.should have_selector('title', :text => ' | Subjects')
		end	
  end

	describe "Levels page" do
		it "should have h1 'Add Level Information'" do
			visit '/admin_pages/levels'
			page.should have_content('Add Level Information')
		end
		it "should have the title 'Levels'" do
			visit '/admin_pages/levels'
			page.should have_selector('title', :text => ' | Levels')
		end	
  end


end
