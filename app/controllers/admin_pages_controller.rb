class AdminPagesController < ApplicationController

  def index
  	@title = 'Home'
    @subjects = Subject.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @subjects }
    end
  end


end
