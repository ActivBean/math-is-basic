class LessonsController < ApplicationController
  def new
    subject = Subject.find(params[:subject_id])  
 	  @lesson = Lesson.new
    @lesson.subject = subject
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @lesson }
    end  	
  end

  def edit
    subject = Subject.find(params[:subject_id])
    @lesson = subject.lessons.find(params[:id])
  end

  def create
    subject= Subject.find(params[:subject_id])
    @lesson = subject.lessons.create!(params[:lesson])
    @lesson.save
    render 'new'
  end

  def update
    subject = Subject.find(params[:subject_id])
    @lesson = subject.lessons.find(params[:id])
    @lesson.update_attributes(params[:lesson])
    render 'edit'
  end


  def destroy
    @subject= Subject.find(params[:subject_id])
    @lesson = @subject.lessons.find(params[:id])
    @lesson.destroy
    respond_to do |format|
      format.html { redirect_to edit_subject_path(@subject) }
      format.json { head :no_content }
    end

  end
  
end
