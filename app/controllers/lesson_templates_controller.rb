class LessonTemplatesController < ApplicationController
  def new
    @lesson_template = LessonTemplate.new
    subject = Subject.find(params[:subject_id])
  	lesson = subject.lessons.find(params[:lesson_id])
    @lesson_template.lesson = lesson
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @lesson_template }
    end  	

  end

  def edit
    subject = Subject.find(params[:subject_id])
  	lesson = subject.lessons.find(params[:lesson_id])
  	@lesson_template = lesson.lesson_templates.find(params[:id])
  end

  def create
    subject = Subject.find(params[:subject_id])
    lesson = subject.lessons.find(params[:lesson_id])
    @lesson_template = lesson.lesson_templates.create!(params[:lesson_template])
    @lesson_template.save
    render 'new'
  end


  def update
    subject = Subject.find(params[:subject_id])
    lesson = subject.lessons.find(params[:lesson_id])
    @lesson_template = lesson.lesson_templates.find(params[:id])
    @lesson_template.update_attributes(params[:lesson_template])
    render 'edit'
  end


  def destroy
    subject = Subject.find(params[:subject_id])
    lesson = subject.lessons.find(params[:lesson_id])
    @lesson_template = lesson.lesson_templates.find(params[:id])
    @lesson_template.destroy
    respond_to do |format|
      format.html { redirect_to edit_lesson_path(:id => lesson.id, :subject_id => subject.id) }
      format.json { head :no_content }
    end
  end

end
