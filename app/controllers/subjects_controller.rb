class SubjectsController < ApplicationController

  def new
    @subject = Subject.new
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @subject }
    end
  end

  def edit
    @subject = Subject.find(params[:id])
  end

  def create
    @subject = Subject.new(params[:subject])
    @subject.save
    render 'new'
  end

  def update
    @subject = Subject.find(params[:id])
    @subject.update_attributes(params[:subject])
    render 'edit'
  end

  def destroy
    @subject = Subject.find(params[:id])
    @subject.destroy
    respond_to do |format|
      format.html { redirect_to admin_pages_path }
      format.json { head :no_content }
    end
  end


end
