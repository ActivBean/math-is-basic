#require 'json'
class HomeController < ApplicationController


  def index
      subject = Subject.find_by(name: "Elementary Maths")
      lessonStartLevel = subject.maxLessonLevel / 2
      lesson = subject.lessons.find_by(:level => lessonStartLevel)
      startTemplateLevel = lesson.maxLessonTemplateLevel / 2
      lessonTemplate = lesson.lesson_templates.find_by(:templateLevel => startTemplateLevel)
      @mode = "Engage"
      body = lessonTemplate.body
      session[:problem] = SingleDigitAdditionTemplate.new
      @problem = session[:problem]
      @problem_title =  SingleDigitAdditionTemplate::TITLE
  end    

  def solve
    problem = session[:problem]
    answer = problem.answer
    for i in 0..problem.answer.size - 1
      var = "st" + i.to_s 
      if problem.answer[i].to_s != params[var].to_s
        puts '####################'
        puts problem.answer[i].to_s
        puts params[var].to_s
        puts '####################'
        @problem = session[:problem]
        #if user_signed_in
        #  report_user(current_user)
        # end
        render :index
        return
      end    
      i = i + 1
    end  
    redirect_to :root
 end

end
