class UsersController < Devise::RegistrationsController
  def create
    @user = User.new(params[:user])
    if @user.save
        redirect_to :root
    else
    	render 'new'
    end
    
  end

  def index
  end
  
end
