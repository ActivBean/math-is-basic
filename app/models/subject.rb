class Subject
  include Mongoid::Document
  field :name, type: String
  field :maxLessonLevel, type: Integer, :default => 0
  field :minLessonLevel, type: Integer, :default => 0
  validates_uniqueness_of :name
  validates_presence_of :name
  embeds_many :lessons, :cascade_callbacks=> true
end
