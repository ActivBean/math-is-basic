class SingleDigitAdditionTemplate
	include Mongoid::Document
  
  TITLE =  "Single Digit Addition" 
  
  field :number1, type: Integer
  field :number2, type: Integer  
  field :step, type: Array
  field :answer, type: Array
  field :example, type: Array

  attr_accessor :step, :answer, :example, :number1, :number2

  def initialize
  	prng = Random.new
  	self.number1 = prng.rand(1..10)
  	self.number2 = prng.rand(1..10)
  	self.answer = Array.new(5)
  	self.example = Array.new(5)
  	self.step = Array.new(5)
  	setup_solution(self.number1, self.number2, self.answer)
  	setup_example(prng.rand(1..9), prng.rand(1..9), self.example)
  end	


  def setup_solution(m, n, arr)
  	arr[0] = m.to_s
  	arr[1] = '+'
  	arr[2] = n.to_s
  	arr[3] = '='
  	arr[4] = (m + n).to_s
  end
  
  def setup_example(m, n, arr)
  	if( m == number1 && n == number2)
  		prng = Random.new
  		setup_example(prng.rand(1..10), prng.rand(1..10), example)
  	else
  		setup_solution(m, n, example)	
  	end	
  end	

  def save;
  end


end  
