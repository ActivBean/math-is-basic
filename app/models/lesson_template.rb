class LessonTemplate
  include Mongoid::Document
  field :name, type: String
  field :templateLevel, type: Integer, :default => 0
  field :body, type: String
  embedded_in :lesson
  validates_presence_of :name
end
