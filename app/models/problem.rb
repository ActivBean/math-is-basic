class Problem
	attr_accessor :title, :type, :variables, :steps, :problem, :actual_steps

	def initialize(title, type, variables, steps, problem, actual_steps)
		@title = title
		@type = type
		@variables = variables
		@steps = steps
		@problem = problem
		@actual_steps = actual_steps
	end
	

end