class Lesson
  include Mongoid::Document
  field :name, type: String
  field :level, type: Integer
  field :maxLessonTemplateLevel, type: Integer, :default => 0
  field :minLessonTemplateLevel, type: Integer, :default => 0
  embedded_in :subject,:inverse_of => :subjects
  embeds_many :lesson_templates, :cascade_callbacks=> true
  validates_uniqueness_of :name
  validates_presence_of :name
end
